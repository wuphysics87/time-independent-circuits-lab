#!/usr/bin/env python3
import matplotlib
import numpy
import plotly.graph_objects as go
import matplotlib.pyplot as plt
from pylab import *
from matplotlib import *
import sys

sys.path.insert(1, 'data')

def data_choice():
    print("\n============================")
    print("Choose Your Dataset Number")
    print("Red Resistor..........1")
    print("Tan Resistor..........2")
    print("Blue Resistor.........3")
    print("Series Resistors......4")
    print("Parallel Resistors....5")
    print("============================\n")

def scatter_plot():
    data_choice()
    data_set = input(": ")
    if data_set in ['1', '2', '3']:
        scatter='scatter-' + data_set

        plot_data = __import__(scatter, fromlist=["main"])
        x = plot_data.current
        y = plot_data.voltage
        plt.scatter(x, y)
        plt.plot(np.unique(x), np.poly1d(np.polyfit(x, y, 1))(np.unique(x)))
        plt.savefig('plots/' + scatter + '.png')
        plt.show()
        choose_again()
    else:
        print("\nIllegal choice! Pick again!\n")
        scatter_plot()

def contour_plot():
    data_choice()
    data_set = input(": ")

    if data_set in ['1', '2', '3', '4', '5']:
        sheet='sheet-' + data_set
        plot_data = __import__(sheet, fromlist=["main"])
        fig = go.Figure(data = go.Contour(z=plot_data.my_data))
        fig.show()
        choose_again()
    else:
        print("\nIllegal choice! Pick again!\n")
        contour_plot()

scatter_plot()
