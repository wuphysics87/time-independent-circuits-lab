<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [Time-Independent Circuits Lab](#time-independent-circuits-lab)
    - [Windows Config](#windows-config)
        - [Pinning Our repos Directory](#pinning-our-repos-directory)
        - [Patching](#patching)
    - [Cloning Repos](#cloning-repos)
    - [Today's Setup](#todays-setup)
    - [The Lab](#the-lab)
        - [Multimeter Measurements](#multimeter-measurements)
        - [Experimental Measurements](#experimental-measurements)
        - [A Little Bit of Git](#a-little-bit-of-git)

<!-- markdown-toc end -->

# Time-Independent Circuits Lab

Open this repository on your lab computer or phone so it's easy to follow along.

Windows users will need to do some configuration. Continue to the next section.    

MacOS Folks, hop down to [Cloning Repos](#cloning-repos). 

## Windows Config

### Pinning Our repos Directory

1. Open **File Explorer** by pressing *win + e*. Remember this. It's the fastest way to do it.
2. On the side panel, go to *This PC* (Usually at the bottom). Then *Local Disk*
3. Then *Users > username > Documents*  right click on *repos* and click 'Pin to Quick Access'

![Find Repos](images/find-repos.jpg)

4. Click on 'Home' on the left panel.

![Pinned Directory](images/pinned-directory.jpg)

5. Click on 'Home' and **delete everything**. You'll be re-cloning everything.

### Patching 

1. Open **Codium**
2. Press *ctrl + shfit + p* on Windows  or  *cmd + shfit + p* on MacOS to open the command pallet.
3. Type `git clone`, press `enter`, and copy and paste

    ```
    https://gitlab.com/wuphysics87/hello-world
    ```

6. Open your start menu and click on 'Ubuntu 22.04.2'
7. Paste by right clicking in your terminal. Press `return` to run.
8. Ask for help if anything goes wrong.


## Cloning Repos

If you on MacOS: 

1. Open your terminal
2. Navigate to *Documents* 
3. Type `pwd`. You should see something like *Users/username/Documents*. 
4. Then run `rm -rf repos/*`

For everyone:

1. Open **Codium**
2. Press *ctrl + shfit + p* on Windows  or  *cmd + shfit + p* on MacOS to open the command pallet.
3. Type `git clone`, press `enter`, and copy and paste

    ```
    https://gitlab.com/wuphysics87/time-independendent-ciruits-lab
    ```

4. When the file explorer opens *repos* directory. 
5. Click 'Select as Repository Destination', then open repository
6. Repeat steps 3-5 for:

    ```
    https://gitlab.com/wuphysics87/hello-bitwarden
    ```

    ```
    https://gitlab.com/wuphysics87/hello-gitlab
    ```


## Today's Setup

If you are on MacOS you'll be ahead of your Windows colleagues. Read ahead or play around with [hello-world](wluhello.world), but wait until we are all ready to continue. 

* https://gitlab.com/wuphysics87/hello-bitwarden 
* https://gitlab.com/wuphysics87/hello-gitlab

## The Lab

In this lab, we will be collecting data to confirm the macroscopic form of Ohm's Law. Namely:

> V=IR

We will be determining the resistances of 3 resistors: 1 tan with a red stripe, a second tan, and a blue one. Measure them using the digital multimeter configured as:

![multi r](images/multi-r.jpeg)

### Multimeter Measurements
Record the values in this table:

|scatter| resistor      | resistance (ohms)    |
|-|---------------|----------------------|
|1| Red           | ???                  |
|2| Tan           | ???                  |
|3| Blue          | ???                  |
|4| Series        | ???                  |
|5| Parallel      | ???                  |

Where the series and parallel cases are using the *Red* and *Tan* resistors

![multi r](images/multi-r.jpeg)

### Experimental Measurements

To run the plotting code, we need to install the python dependancies.

``` sh
pip install -r requirements.txt
```

You may need to run `pip3` instead of `pip` if this command doesn't work.

For each resistor scenario, set the power supply to 5 different voltages. Record your voltages and currents in their respective *data/scatter*.

When you are ready to generate a plot. Run:

``` sh
python3 plot.py
```

### A Little Bit of Git

1. Check all of my commits by typing `git log` in the terminal. 

Take note of the ways these are writen. Standard commits are written in present tense and are between 3 and 10 words.  **DO NOT** write commit messages like "Update" or "Fix". These are not helpful for you or anyone else who looks at the code later.

2. To get the status of this repository, type `git status`
3. Stage your code with `git add *`
4. Create a commit with `git commit -m "A GOOD COMMIT MESSAGE FOR WHAT YOU DID"`
5. To push to gitlab type `git push origin main`

